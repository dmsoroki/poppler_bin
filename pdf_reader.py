from __future__ import print_function

import subprocess
import sys


def convert(fname):
	result = subprocess.check_output(['bin/pdftotext.exe', '-enc', 'UTF-8', '-layout', str(fname), '-'])
	return result.decode('utf-8', errors='ignore')


if __name__ == '__main__':
	fname = sys.argv[1]
	text = convert(fname)
	print(type(text), text)

